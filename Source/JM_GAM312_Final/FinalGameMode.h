// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FinalGameMode.generated.h"

/**
 * 
 */
UCLASS()
class JM_GAM312_FINAL_API AFinalGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
		virtual void BeginPlay() override;

public:
	AFinalGameMode();


protected:

	//Allows the blueprints to be seen in the editor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stamina", Meta = (BlueprintProtected = "true"))

		TSubclassOf<class UUserWidget> PlayerHUDClass;

	UPROPERTY()
		class UUserWidget* CurrentWidget;
	};
