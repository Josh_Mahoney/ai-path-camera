// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Engine.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "Kismet/GameplayStatics.h"
#include "PathRunnerController.generated.h"

/**
 * 
 */
UCLASS()
class JM_GAM312_FINAL_API APathRunnerController : public AAIController
{
	GENERATED_BODY()
	
public:
	void BeginPlay() override;

	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

private:
	UPROPERTY()
		TArray<AActor*> Waypoints;
	//Needed for waiting at each checkpoint
	FTimerHandle TimerHandle;

	UFUNCTION()
		//Needed to set up movement from one checkpoint to the next
		void GoToNextWaypoint();
	//Allows us to set the number of checkpoints in the game engine
	int currentWaypointIndex = 0;
	int WaypointIndexCount = 0;

};

