// Fill out your copyright notice in the Description page of Project Settings.

#include "ComponentP.h"
#include "Components/InputComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "ConstructorHelpers.h"
#include "MyPawnMovementComponent.h"

// Sets default values
AComponentP::AComponentP()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	
	//This section sets up our object and creates the parameters for picking it up

	PrimaryActorTick.bCanEverTick = true;

	USphereComponent* SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	RootComponent = SphereComponent;
	SphereComponent->InitSphereRadius(40.f);
	SphereComponent->SetCollisionProfileName(TEXT("Pawn"));

	UStaticMeshComponent* SphereMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visual"));
	SphereMesh->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh>SphereMesh1Component(TEXT("/Game/StarterContent/Shape_Sphere.Shape_Sphere"));
	if (SphereMesh1Component.Succeeded())
	{
		SphereMesh->SetStaticMesh(SphereMesh1Component.Object);
		SphereMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -40.0f));
		SphereMesh->SetWorldScale3D(FVector(0.0f));
	}

	AutoPossessPlayer = EAutoReceiveInput::Player0;

	PathRunnerMovementComponent = CreateDefaultSubobject<UMyPawnMovementComponent>(TEXT("CustomMovements"));
	PathRunnerMovementComponent->UpdatedComponent = RootComponent;
}



// Called when the game starts or when spawned
void AComponentP::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AComponentP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AComponentP::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("Lateral", this, &AComponentP::Lateral);
	InputComponent->BindAxis("SideToSide", this, &AComponentP::SideToSide);

}
//Sets up the lateral movement for our pawn
void AComponentP::Lateral(float value)
{
	if (PathRunnerMovementComponent && (PathRunnerMovementComponent->UpdatedComponent == RootComponent))
	{
		PathRunnerMovementComponent->AddInputVector(GetActorForwardVector() * value);
	}
}
//Sets up side to side movement for our pawn
void AComponentP::SideToSide(float value)
{
	if (PathRunnerMovementComponent && (PathRunnerMovementComponent->UpdatedComponent == RootComponent))
	{
		PathRunnerMovementComponent->AddInputVector(GetActorRightVector() * value);
	}
}

UPawnMovementComponent* AComponentP::GetMovementComponent() const
{
	return PathRunnerMovementComponent;
}

