// Fill out your copyright notice in the Description page of Project Settings.

#include "PathRunnerController.h"


//On game start gets all classes with this AI controller and number of waypoints
void APathRunnerController::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), Waypoints);

	WaypointIndexCount = Waypoints.Num();

	GoToNextWaypoint();
}

//Sets up how long the actor waits until going on to the next checkpoint
void APathRunnerController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	GetWorldTimerManager().SetTimer(TimerHandle, this, &APathRunnerController::GoToNextWaypoint, 3.0f, false);
}

//Allows the actors to go onto the next waypoint, sets up the loop of checkpoints to follow(Order you put or number checkpoints in engine)
void APathRunnerController::GoToNextWaypoint()
{
	MoveToActor(Waypoints[currentWaypointIndex]);

	int Cycle = currentWaypointIndex % WaypointIndexCount;
	if (currentWaypointIndex >= WaypointIndexCount - 1)
	{
		currentWaypointIndex = 0;
	}
	else
	{
		currentWaypointIndex++;
	}
}
