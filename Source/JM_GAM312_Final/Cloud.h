// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Cloud.generated.h"

UCLASS()
class JM_GAM312_FINAL_API ACloud : public ACharacter
{
	GENERATED_BODY()
public:
	ACloud();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Sets up the Forward and Backward
	void Lateral(float Value);

	//Sets up the Side to Side
	void SidetoSide(float Value);

	void SprintOn();

	void SprintOff();

	bool IsSprinting = false;


	//Attempt to get Pitch and Yaw working
	void MouseYaw(float axis);
	void MousePitch(float axis);
	
	FVector2D mouseInput;



};