// Fill out your copyright notice in the Description page of Project Settings.

#include "MainCharacter.h"
#include "Components/InputComponent.h" 
#include "Kismet/GameplayStatics.h"  
#include "Camera/CameraComponent.h"  
#include "GameFramework/InputSettings.h"  
#include "Components/CapsuleComponent.h"  


// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	//Sets up the initial and current stanima values
	InitialStamina = 100.f;
	CurrentStamina = InitialStamina;

}
//Sets up how fast we lose stamina 
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateCurrentStamina(-DeltaTime * 0.05f * (InitialStamina));
}

//Gets our initial/startup stamina value
float AMainCharacter::GetInitialStamina()
{
	return InitialStamina;
}
//Gets our current stamina value
float AMainCharacter::GetCurrentStamina()
{
	return CurrentStamina;
}
//Displays the current stamina
void AMainCharacter::UpdateCurrentStamina(float Stamina)
{
	CurrentStamina = CurrentStamina + Stamina;
}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Binds the inputs for the lateral and side to side functions (W,A,S,D)
	InputComponent->BindAxis("Lateral", this, &AMainCharacter::Lateral);
	InputComponent->BindAxis("SidetoSide", this, &AMainCharacter::SidetoSide);
	//Sets up the sprint function (Left Shift)
	InputComponent->BindAction("Sprint", IE_Pressed, this, &AMainCharacter::SprintOn);
	InputComponent->BindAction("Sprint", IE_Released, this, &AMainCharacter::SprintOff);
	//Sets up the inputs for Turn and LookUp functions (Mouse Control)
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &AMainCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &AMainCharacter::LookUpAtRate);

}
void AMainCharacter::Lateral(float Value)
{
	if (Controller && Value)
		if (IsSprinting)
			Value *= 3;
	{
		FVector Forward = GetActorForwardVector();
		AddMovementInput(Forward, Value / 3);
	}
}
//Stes up movement for SideToSide w/ and w/o sprint
void AMainCharacter::SidetoSide(float Value)
{
	if (Controller && Value)
		if (IsSprinting)
			Value *= 3;
	{
		FVector Right = GetActorRightVector();
		AddMovementInput(Right, Value / 3);
	}
}

// Function definition for changing the movement speed if sprint is activated
void AMainCharacter::SprintOn()
{
	IsSprinting = true;

}

// Function definition for setting the movement speed back to default when key is released
void AMainCharacter::SprintOff()
{
	IsSprinting = false;
}

void AMainCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMainCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}