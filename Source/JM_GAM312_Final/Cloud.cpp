// Fill out your copyright notice in the Description page of Project Settings.

#include "Cloud.h"
#include "GameFramework/Pawn.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/FloatingPawnMovement.h"



// Sets default values
ACloud::ACloud()
{
	PrimaryActorTick.bCanEverTick = true;
		
}

// Called when the game starts or when spawned
void ACloud::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACloud::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Testing to get Yaw and Pitch working
	FRotator newYaw = GetActorRotation();
	FRotator newPitch = GetActorRotation();
	newYaw.Yaw += mouseInput.X;
	newPitch.Pitch += mouseInput.Y;
	SetActorRotation(newYaw);
	SetActorRotation(newPitch);
}
// Called to bind functionality to input
void ACloud::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Binds the inputs for the lateral and side to side functions
	InputComponent->BindAxis("Lateral", this, &ACloud::Lateral);
	InputComponent->BindAxis("SidetoSide", this, &ACloud::SidetoSide);
	//Sets up the sprint function
	InputComponent->BindAction("Sprint", IE_Pressed, this, &ACloud::SprintOn);
	InputComponent->BindAction("Sprint", IE_Released, this, &ACloud::SprintOff);

	//Attempt to get Yaw and Pitch bound
	InputComponent->BindAxis("MouseYaw", this, &ACloud::MouseYaw);
	InputComponent->BindAxis("MousePitch", this, &ACloud::MousePitch);
}

//Sets up Lateral movement w/ and w/o sprint
void ACloud::Lateral(float Value)
{
	if (Controller && Value)
		if (IsSprinting)
			Value *= 3;
	{
		FVector Forward = GetActorForwardVector();
		AddMovementInput(Forward, Value / 3);
	}
}
//Stes up movement for SideToSide w/ and w/o sprint
void ACloud::SidetoSide(float Value)
{
	if (Controller && Value)
		if (IsSprinting)
			Value *= 3;
	{
		FVector Right = GetActorRightVector();
		AddMovementInput(Right, Value / 3);
	}
}

// Function definition for changing the movement speed if sprint is activated
void ACloud::SprintOn()
{
	IsSprinting = true;

}

// Function definition for setting the movement speed back to default when key is released
void ACloud::SprintOff()
{
	IsSprinting = false;
}

//The following is to try and get function for Yaw and Pitch
void ACloud::MouseYaw (float axis)
{
	
	mouseInput.X = axis;
}
void ACloud::MousePitch (float axis)
{
	
	mouseInput.Y = axis;
}
