// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainCharacter.generated.h"

UCLASS()
class JM_GAM312_FINAL_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

public:
	// Sets default values for this character's properties
	AMainCharacter();

	//Sets up the tick time for stanima
	virtual void Tick(float DeltaTime) override;

	//Function for initial stamina
	UFUNCTION(BlueprintPure, Category = "Stamina")
		float GetInitialStamina();

	//Function for initial stanima
	UFUNCTION(BlueprintPure, Category = "Stamina")
		float GetCurrentStamina();

	//Upates players current stamina
	UFUNCTION(BlueprintCallable, Category = "Stamina")
		void UpdateCurrentStamina(float Stamina);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	//Sets up the turn rate for the players mouse controls
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	//Sets up the LookUp rate for the players mouse controls
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Sets up the Forward and Backward
	void Lateral(float Value);

	//Sets up the Side to Side
	void SidetoSide(float Value);

	//Sets up the condition for sprinting
	void SprintOn();

	void SprintOff();

	bool IsSprinting = false;

	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);

private:
	//Sets up starting and current stamina values
	UPROPERTY(EditAnywhere, Category = "Stamina")
		float InitialStamina;

	UPROPERTY(EditAnywhere, Category = "Stamina")
		float CurrentStamina;

	
};
