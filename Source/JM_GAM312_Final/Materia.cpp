// Fill out your copyright notice in the Description page of Project Settings.

#include "Materia.h"
#include "DrawDebugHelpers.h"


// Sets default values
AMateria::AMateria()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//Sets up the material to the RootCOmponent
	MateriaRoot = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent = MateriaRoot;
	//Sets the material to our static mesh so it can have a visual identity
	MateriaMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	MateriaMesh->AttachToComponent(MateriaRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);
	//Sets up the bank to store the objects once we begon the overlap that allows for a message and destroction of the object
	MateriaBank = CreateDefaultSubobject<UBoxComponent>(TEXT("MateriaBank"));
	MateriaBank->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
	MateriaBank->AttachToComponent(MateriaRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);
	MateriaBank->OnComponentBeginOverlap.AddDynamic(this, &AMateria::OnOverlapBegin);
	MateriaBank->OnComponentEndOverlap.AddDynamic(this, &AMateria::OnOverlapEnd);
}

// Called when the game starts or when spawned
void AMateria::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMateria::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMateria::OnOverlapBegin(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResults)
{
	//Sets up the message we see when we begin our overlap of the object
	DrawDebugString(GetWorld(), GetActorLocation(), "You found Materia!!", nullptr, FColor::White, 5.0f, false);
}

void AMateria::OnOverlapEnd(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex)
{
	//Sets up messgae when we end our overlap and destroy the object
	DrawDebugString(GetWorld(), GetActorLocation(), "Your Materia has been added!!", nullptr, FColor::White, 5.0f, false);
	Destroy();
}
