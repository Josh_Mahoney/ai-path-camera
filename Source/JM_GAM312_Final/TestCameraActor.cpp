// Fill out your copyright notice in the Description page of Project Settings.

#include "TestCameraActor.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ATestCameraActor::ATestCameraActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATestCameraActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATestCameraActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Setting up the conditions of the camera change *Not working Staying blurred on Camera 1 Messed around with blend and CamerChanges but little to no effect
	const float TimeBetweenCameraChanges = 5.0f;
	const float SmoothBlendTime = 2.0f;
	CameraSwitchTime -= DeltaTime;
	if (CameraSwitchTime <= 0.0f)
	{
		CameraSwitchTime += CameraSwitchTime;

		APlayerController* OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
		if (OurPlayerController)
		{
			if ((OurPlayerController->GetViewTarget() != CameraOne) && (CameraOne != nullptr))
			{
				//Sets the game view to the first camera
				OurPlayerController->SetViewTarget(CameraOne);
			}
			else if ((OurPlayerController->GetViewTarget() != CameraTwo) && (CameraTwo != nullptr))
			{
				//Sets up the view for camera 2
				OurPlayerController->SetViewTargetWithBlend(CameraTwo, SmoothBlendTime);
			}
		}

	}
}
