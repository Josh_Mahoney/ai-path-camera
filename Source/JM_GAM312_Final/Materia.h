// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DrawDebugHelpers.h"
#include "Engine.h"
#include "Materia.generated.h"

UCLASS()
class JM_GAM312_FINAL_API AMateria : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMateria();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Sets up the root component
	UPROPERTY(EditAnywhere)
		USceneComponent* MateriaRoot;

	//Sets up the static mesh component
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* MateriaMesh;

	//Sets up the bank to stare the components once picked up
	UPROPERTY(EditAnywhere)
		UShapeComponent* MateriaBank;
	//Sets up the overlap function when you first contact the item
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResults);
	//Sets up the overlap function in order to destroy the component to end the overlap
	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex);
};
	
	
