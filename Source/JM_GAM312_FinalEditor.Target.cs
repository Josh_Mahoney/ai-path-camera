// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class JM_GAM312_FinalEditorTarget : TargetRules
{
	public JM_GAM312_FinalEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "JM_GAM312_Final" } );
	}
}
